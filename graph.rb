require 'byebug'

def depkey(name, data)
  "#{name} #{data['version']}".strip
end

def get_graph(json)
  if json.has_key?('packages') 
     get_graph_from_v2(json)
   else
     get_graph_from_v1(json)
   end
end

def get_graph_from_v1(json)
  result = {}
  json['dependencies'].each do |name, pkgdata|
    key = depkey(name, pkgdata['version'] || '')
    result[key] ||= []

    next unless pkgdata.has_key?('requires')

    pkgdata['requires'].each do |name, version|
      childkey = depkey(name, version)
      result[key] << childkey
    end
  end
  result
end

def get_graph_from_v2(json)
  result = {}
  json['packages'].each do |name, pkgdata|
    key = depkey(name, pkgdata['version'] || '')
    result[key] ||= []

    next unless pkgdata.has_key?('dependencies')

    pkgdata['dependencies'].each do |name, version|
      childkey = depkey(name, version)
      result[key] << childkey
    end

    next unless pkgdata.has_key?('devDependencies')

    pkgdata['devDependencies'].each do |name, version|
      childkey = depkey(name, version)
      result[key] << childkey
    end
  end
  result
end

def analyze_file(file)
  c = File.read(file)
  return unless c.length > 0
  j = JSON.parse(c)
  adjacency_list = get_graph(j)

  repo, stars =file.scan(/^([^_]+)_package-lock.json_(\d+)$/).first
  repo.gsub!('--','/')

  ndeps = adjacency_list.count
  nedges = adjacency_list.inject(0) do |res,edges|
    res += edges.count
  end
  nnz = adjacency_list.inject(0) do |res, tup|
    res += tup.last.count > 0 ? 1 : 0
  end

  return repo, stars, ndeps, nedges, nnz
end
