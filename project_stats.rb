require 'json'

load 'graph.rb'

def link_repo(repo)
  "https://github.com/#{repo}"
end

puts '| repo | stars | num deps | num with subdeps | num edges | adjacency list space | pct sparse | sparse matrix compressed row space |'
puts '| ---  |  ---  |  ---     | ---              | ---       | ---                  | ---        | ---                                |'
#puts '| repo | stars | num deps | num with subdeps | num edges |'
#puts '| ---  |  ---  |  ---     | ---              | ---       |'

adj_entry_bits=4*8
csc_entry_bits=3

c=0
stats = Dir.glob('*_package-lock.json_*').map do |file|
  repo, stars, ndeps, nedges, nnz = analyze_file(file)
  next if repo.nil?
  sparseness = sprintf("%.8f",(1-(nedges*1.0/(ndeps*ndeps)))*100)
  adj_list_space = (nedges*adj_entry_bits)/1024
  csc_space = ((2.0*nnz + ndeps +1)*csc_entry_bits)/1024
  puts "| #{link_repo(repo)} | #{stars} | #{ndeps} | #{nnz} | #{nedges} | #{adj_list_space}kb | #{sparseness}% | #{csc_space}kb |"
  #puts "| #{link_repo(repo)} | #{stars} | #{ndeps} | #{nnz} | #{nedges} |"
  c+=1
end

